import base64

f = open('dump_file.txt', "r")

lines = f.readlines()

f.close()

decoding = [""]

for i in range(len(lines)):
    tr_line = str(base64.standard_b64decode(lines[i]))
    decoding.append(tr_line)

file = open('translated_file.txt',"w")

for j in range(len(decoding)):
    file.write(str(decoding[j]))

file.close()